/*
Code refactoring:

 - refactor the code in OOP-code.
 - if/else-, switch/case, ?- Operators are forbidden
 - try to use generics.
 - create useful junit-test
*/


import java.util.HashMap;
import java.util.Map;

interface Counter {
    <T> int customAction(T num1, T num2);
}

public class Main {

    public static void main(String[] args) {
        Main main = new Main();

        System.out.println(main.count(6, 4, "+"));
        System.out.println(main.count(6, 4, "-"));
        System.out.println(main.count(6, 4, "*"));
        System.out.println(main.count(6, 4, "/"));
        System.out.println(main.count("6", "4", "+"));


    }

    <T> int count(T num1, T num2, String action) {
        Map<String, Counter> commandsMap = initializeCommandsMap();

        return commandsMap.get(action).customAction(num1, num2);

    }

    private static Map<String, Counter> initializeCommandsMap() {

        Map<String, Counter> commandsMap = new HashMap<>();

        commandsMap.put("+", new Counter() {
            @Override
            public <T> int customAction(T num1, T num2) {

                if (num1 instanceof Integer) {
                    return (Integer) num1 + (Integer) num2;
                }

                return Integer.parseInt(num1 + (String) num2);

            }

        });

        commandsMap.put("-", new Counter() {
            @Override
            public <T> int customAction(T num1, T num2) {
                return ((Integer) num1 - ((Integer) num2));
            }
        });

        commandsMap.put("/", new Counter() {
            @Override
            public <T> int customAction(T num1, T num2) {
                int number2 = (Integer) num2;

                if (number2==0) return 0;

                return ((Integer) num1 / (number2));
            }

        });

        commandsMap.put("*", new Counter() {
            @Override
            public <T> int customAction(T num1, T num2) {
                return ((Integer) num1) * ((Integer) num2);
            }

        });

        return commandsMap;
    }

}

