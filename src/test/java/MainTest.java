import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MainTest {

    private Main main;

    @Before
    public void setUp() {
        main = new Main();
    }

    @Test
    public void count() {
        /* plus operation */

        Assert.assertEquals(main.count(-5, 2, "+"), -3);
        Assert.assertEquals(main.count(2, -5, "+"), -3);



        Assert.assertEquals(main.count(0, 5, "+"),5);
        Assert.assertEquals(main.count(5, 0, "+"),5);

        /* minus operation */

        Assert.assertEquals(main.count(-5, 2, "-"),-7);
        Assert.assertEquals(main.count(2, -5, "-"),7);

        Assert.assertEquals(main.count(0, 5, "-"),-5);
        Assert.assertEquals(main.count(5, 0, "-"),5);


        /* multiply operation */

        Assert.assertEquals(main.count(-5, 2, "*"),-10);
        Assert.assertEquals(main.count(2, -5, "*"),-10);

        Assert.assertEquals(main.count(0, 5, "*"),0);
        Assert.assertEquals(main.count(5, 0, "*"),0);


        /* divide operation */

        Assert.assertEquals(main.count(-5, 2, "/"),-2);
        Assert.assertEquals(main.count(2, -5, "/"),-0);

        Assert.assertEquals(main.count(0, 5, "/"),0);
        Assert.assertEquals(main.count(5, 0, "/"),0);//exception

    }
}